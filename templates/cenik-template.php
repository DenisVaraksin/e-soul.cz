<?php
/*
Template Name: cenik
Template Post Type: page
*/
get_header(); ?>

    <!-- First Table -->

<?php
$args = array(
    'post_type' => array('cenik'), // post type we display
    'order' => 'DESC',
    'posts_per_page' => 1,
    "orderby" => "menu_order",
    "post_status" => "publish",
);
$loop3 = new WP_Query($args);
while ($loop3->have_posts()) {
    $loop3->the_post();
    ?>

    <section class="cenik_table_section" id="c_over_firstpart">
        <div class="section"
             style="background:url(<?php echo get_template_directory_uri() . "/img/cenik_background.png" ?>) no-repeat; background-position:center 32%; background-size: 90%;">
            <div class="container" id="c_first_container">
                <div class="row">
                    <?php
                    //Data from ACF
                    $cenik_title = get_field('first_title');
                    $first_block = get_field('first_block');
                    $second_block = get_field('second_block');
                    $third_block = get_field('third_block');
                    $shit_price = get_field('shit_price');
                    $pro_price = get_field('pro_price');
                    $poplatek = get_field('poplatek');
                    $eshop_funkce = get_field('eshop_funkce');
                    $order_demo_title = get_field("last_title");
                    ?>
                    <div class="col">
                        <div class="cenik_title text-center">
                            <?php echo $cenik_title; ?>
                        </div>
                        <?php
                        if (current_user_can('administrator')) {
                            ?>
                            <h3 class="text-center"><b>Vyberte si variantu ceníku</b></h3>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <?php

                $table1 = '<div class="overflow_table">
                <table class="table" id="c_first_table">
                    <thead>
                    <tr class="header_row_title">
                        <th scope="col" id="table_first_row" class="wid-25">
                        </th>
                        <th scope="col" class="wid-25" id="c_first_th">
                            <div class="c_first_block">
                                <div>' . $first_block . '</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_second_th">
                            <div class="c_second_block">
                                <div>' . $second_block . '</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_third_th">
                            <div class="c_first_block">
                                <div>' . $third_block . '</div>
                            </div>
                        </th>
                    </tr>
                    </thead>


                    <thead>
                    <tr class="header_row_bubble">
                        <th scope="col" id="table_first_row" class="wid-25">
                            <div class="header_zakladi start_header">
                                <img src="' . get_stylesheet_directory_uri() . '/img/server.png"/>
                                <p class="small-p"> Specifikace</p>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_first_th">
                            <div class="c_first_block">
                                <div>Plugin s licencí na 1 e-shop a šablona s hotovým designem</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_second_th">
                            <div class="c_second_block">
                                <div>Úpravy šablony na míru s grafikou a kódováním</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_third_th">
                            <div class="c_first_block">
                                <div>Vyber si jen to, co potřebuješ a vytvoříme ti individuální řešení</div>
                            </div>
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    <tr class="even_row">
                        <td scope="row">Pravidelné aktualizace
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Na základě vaší zpětné vazby a trendů E-Soul pravidelně aktualizujeme a rozšiřujeme o nové funkce.</span>
                            </div>
                        </td>
                        <td class="text-center responsive-tr-3"><img
                                    src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/></td>
                        <td class="text-center responsive-tr-2"><img
                                    src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/></td>
                        <td class="text-center responsive-tr-2"><img
                                    src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/></td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">Podpora
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Možná se dostaneš do slepé uličky, nebo chceš konzultovat možnosti rozšíření. Po telefonu, nebo osobně, reagujeme okamžitě.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">CMS WordPress</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Připravená administrace e-shopu
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Během několika minut nastavíš možnosti dopravy, skladování, objednávek, měn, zákazníků, napojení na platební brány a další funkce. Více najdeš na stránce Pro agentury.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Možnost rozšíření o jiné pluginy
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">E-Soul je plně kompatibilní i s jinými pluginy WordPressu.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4"><a href="https://msesoul.marketsoul.cz/" target="_blank">Design - >>připravená šablona<<</a>
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">V ČR je nejvyšší poměr e-shopů na počet obyvatel. Odlišit se moderním a originálním designem je zkrátka nutnost.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center">Na míru</td>
                        <td class="text-center">Na míru</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Hosting
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Chceš využít vlastní hosting? Není problém, náš hosting není podmínkou.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">- Zálohování webu</td>
                        <td class="text-center">Denně</td>
                        <td class="text-center">Denně</td>
                        <td class="text-center">Denně</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row" class="responsive-tr-2">- Caching</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">Doména</td>
                        <td class="text-center">Vlastní</td>
                        <td class="text-center">Vlastní</td>
                        <td class="text-center">Vlastní</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Emailové schránky</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center responsive-tr-4">Neomezeně</td>
                        <td class="text-center">Neomezeně</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">XML feed
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Využij různé pluginy pro WordPress na generování feedu, my zatím budeme pracovat na integrování vlastního řešení, které to všechno ulehčí.</span>
                            </div>
                        </td>
                        <td class="text-center">není integrován</td>
                        <td class="text-center responsive-tr-4">není integrován</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row">Cizí měny (CZK, EUR, GBP, USD)</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Automatická optimalizace obrázků</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-3">Základní nastavení SEO</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row" class="responsive-tr-3">Cookies (ošetření GDPR)</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-3">Jazykové mutace webu</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row"></td>
                        <td class="text-center" id="c_first_td">
                            <div class="c_table_price text-center">
                                14 900<b> (bez DPH)</b>
                            </div>
                            <div class="c_table_poplatek text-center">
                                + *490 Kč/měsíčně
                            </div>
                            <div class="c_table_footer text-center">
                                <b>* </b>Podpora 24/7, aktualizace, hosting, garance řešení problémů
                            </div>
                        </td>
                        <td class="text-center" id="c_second_td">
                            <div class="c_table_price text-center">
                                od 24 900<b> (bez DPH)</b>
                            </div>
                            <div class="c_table_poplatek text-center">
                                + *490 Kč/měsíčně
                            </div>
                            <div class="c_table_footer text-center">
                                <b>* </b>Podpora 24/7, aktualizace, hosting, garance řešení problémů
                            </div>
                        </td>
                        <td class="text-center" id="c_third_td">
                            <a href="'.get_site_url().'/kontakt" class="c_order_demo responsive-tr-2">
                                Kontaktujte nás
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>';

                $table2 = '<div class="overflow_table">
                <table class="table" id="c_first_table">
                    <thead>
                    <tr class="header_row_title">
                        <th scope="col" id="table_first_row" class="wid-25">
                        </th>
                        <th scope="col" class="wid-25" id="c_first_th">
                            <div class="c_first_block">
                                <div>E-Soul Start</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_second_th">
                            <div class="c_second_block">
                                <div>E-Soul Komplet</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_third_th">
                            <div class="c_first_block">
                                <div>E-Soul na míru</div>
                            </div>
                        </th>
                    </tr>
                    </thead>


                    <thead>
                    <tr class="header_row_bubble">
                        <th scope="col" id="table_first_row" class="wid-25">
                            <div class="header_zakladi start_header">
                                <img src="' . get_stylesheet_directory_uri() . '/img/server.png"/>
                                <p class="small-p"> Specifikace</p>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_first_th">
                            <div class="c_first_block">
                                <div>Samostatný plugin s licencí na 1 e-shop</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_second_th">
                            <div class="c_second_block">
                                <div>Design a programování kompletního e-shopu včetně hostingu</div>
                            </div>
                        </th>
                        <th scope="col" class="wid-25" id="c_third_th">
                            <div class="c_first_block">
                                <div>Vyber si jen to, co potřebuješ a vytvoříme ti individuální řešení</div>
                            </div>
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    <tr class="even_row">
                        <td scope="row">Pravidelné aktualizace
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Na základě vaší zpětné vazby a trendů E-Soul pravidelně aktualizujeme a rozšiřujeme o nové funkce.</span>
                            </div>
                        </td>
                        <td class="text-center responsive-tr-3"><img
                                    src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/></td>
                        <td class="text-center responsive-tr-2"><img
                                    src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/></td>
                        <td class="text-center responsive-tr-2"><img
                                    src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/></td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">Podpora
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Možná se dostaneš do slepé uličky, nebo chceš konzultovat možnosti rozšíření. Po telefonu, nebo osobně, reagujeme okamžitě.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">CMS WordPress</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Připravená administrace e-shopu
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Během několika minut nastavíš možnosti dopravy, skladování, objednávek, měn, zákazníků, napojení na platební brány a další funkce. Více najdeš na stránce Pro agentury.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Možnost rozšíření o jiné pluginy
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">E-Soul je plně kompatibilní i s jinými pluginy WordPressu.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">Design webových stránek
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">V ČR je nejvyšší poměr e-shopů na počet obyvatel. Odlišit se moderním a originálním designem je zkrátka nutnost.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center">Na míru</td>
                        <td class="text-center">Na míru</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Hosting
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Chceš využít vlastní hosting? Není problém, náš hosting není podmínkou.</span>
                            </div>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">- Zálohování webu</td>
                        <td class="text-center">Denně</td>
                        <td class="text-center">Denně</td>
                        <td class="text-center">Denně</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row" class="responsive-tr-2">- Caching</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-4">Doména</td>
                        <td class="text-center">Vlastní</td>
                        <td class="text-center">Vlastní</td>
                        <td class="text-center">Vlastní</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Emailové schránky</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center responsive-tr-4">Neomezeně</td>
                        <td class="text-center">Neomezeně</td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">XML feed
                            <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                        class="tooltip2text">Využij různé pluginy pro WordPress na generování feedu, my zatím budeme pracovat na integrování vlastního řešení, které to všechno ulehčí.</span>
                            </div>
                        </td>
                        <td class="text-center">není integrován</td>
                        <td class="text-center responsive-tr-4">není integrován</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row">Cizí měny (CZK, EUR, GBP, USD)</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row">Automatická optimalizace obrázků</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-3">Základní nastavení SEO</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row" class="responsive-tr-3">Cookies (ošetření GDPR)</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="odd_row">
                        <td scope="row" class="responsive-tr-3">Jazykové mutace webu</td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/cancel.png"/>
                        </td>
                        <td class="text-center"><img src="' . get_stylesheet_directory_uri() . '/img/tick2.png"/>
                        </td>
                    </tr>
                    <tr class="even_row">
                        <td scope="row"></td>
                        <td class="text-center" id="c_first_td">
                            <div class="c_table_price text-center">
                                ' . $shit_price . '<b> (bez DPH)</b>
                            </div>
                            <div class="c_table_poplatek text-center">
                                + *490 Kč/měsíčně
                            </div>
                            <div class="c_table_footer text-center">
                                <b>* </b>Podpora 24/7, aktualizace, hosting, garance řešení problémů
                            </div>
                        </td>
                        <td class="text-center" id="c_second_td">
                            <div class="c_table_price text-center">
                                od ' . $pro_price . '<b> (bez DPH)</b>
                            </div>
                            <div class="c_table_poplatek text-center">
                                + *490 Kč/měsíčně
                            </div>
                            <div class="c_table_footer text-center">
                                <b>* </b>Podpora 24/7, aktualizace, hosting, garance řešení problémů
                            </div>
                        </td>
                        <td class="text-center" id="c_third_td">
                            <a href="'.get_site_url().'/kontakt" class="c_order_demo responsive-tr-2">
                                Kontaktujte nás
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>';

                echo get_tabs(
                    array(
                        '<svg class="tab_img" enable-background="new 0 0 60 60" height="512" viewBox="0 0 60 60" width="512" xmlns="http://www.w3.org/2000/svg">
    <path d="m55.5 60h-51c-2.481 0-4.5-2.019-4.5-4.5v-51c0-2.481 2.019-4.5 4.5-4.5h51c2.481 0 4.5 2.019 4.5 4.5v51c0 2.481-2.019 4.5-4.5 4.5zm-51-58c-1.378 0-2.5 1.121-2.5 2.5v51c0 1.379 1.122 2.5 2.5 2.5h51c1.379 0 2.5-1.121 2.5-2.5v-51c0-1.379-1.122-2.5-2.5-2.5z"/>
    <path d="m9 10h-4c-.552 0-1-.447-1-1v-4c0-.553.448-1 1-1h4c.552 0 1 .447 1 1v4c0 .553-.448 1-1 1zm-3-2h2v-2h-2z"/>
    <path d="m17 10h-4c-.552 0-1-.447-1-1v-4c0-.553.448-1 1-1h4c.552 0 1 .447 1 1v4c0 .553-.448 1-1 1zm-3-2h2v-2h-2z"/>
    <path d="m25 10h-4c-.552 0-1-.447-1-1v-4c0-.553.448-1 1-1h4c.552 0 1 .447 1 1v4c0 .553-.448 1-1 1zm-3-2h2v-2h-2z"/>
    <path d="m59 14h-58c-.552 0-1-.447-1-1v-8.5c0-2.481 2.019-4.5 4.5-4.5h51c2.481 0 4.5 2.019 4.5 4.5v8.5c0 .553-.448 1-1 1zm-57-2h56v-7.5c0-1.379-1.122-2.5-2.5-2.5h-51c-1.378 0-2.5 1.121-2.5 2.5z"/>
    <path d="m7.5 35h-1.5c-.552 0-1-.447-1-1v-1.5c0-.553.448-1 1-1s1 .447 1 1v.5h.5c.552 0 1 .447 1 1s-.448 1-1 1z"/>
    <path d="m6 28.772c-.552 0-1-.447-1-1v-3.546c0-.553.448-1 1-1s1 .447 1 1v3.546c0 .553-.448 1-1 1z"/>
    <path d="m6 20.5c-.552 0-1-.447-1-1v-1.5c0-.553.448-1 1-1h1.5c.552 0 1 .447 1 1s-.448 1-1 1h-.5v.5c0 .553-.448 1-1 1z"/>
    <path d="m48.587 19h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.447 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.447 1-1 1zm-6.847 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1z"/>
    <path d="m54 20.5c-.552 0-1-.447-1-1v-.5h-.5c-.552 0-1-.447-1-1s.448-1 1-1h1.5c.552 0 1 .447 1 1v1.5c0 .553-.448 1-1 1z"/>
    <path d="m54 28.773c-.552 0-1-.447-1-1v-3.546c0-.553.448-1 1-1s1 .447 1 1v3.546c0 .553-.448 1-1 1z"/>
    <path d="m54 35h-1.5c-.552 0-1-.447-1-1s.448-1 1-1h.5v-.5c0-.553.448-1 1-1s1 .447 1 1v1.5c0 .553-.448 1-1 1z"/>
    <path d="m48.587 35h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.447 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.447 1-1 1zm-6.847 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1zm-6.848 0h-2.935c-.552 0-1-.447-1-1s.448-1 1-1h2.935c.552 0 1 .447 1 1s-.448 1-1 1z"/>
    <path d="m30 30c-.552 0-1-.447-1-1v-6c0-.553.448-1 1-1s1 .447 1 1v6c0 .553-.448 1-1 1z"/>
    <path d="m33 27h-6c-.552 0-1-.447-1-1s.448-1 1-1h6c.552 0 1 .447 1 1s-.448 1-1 1z"/>
    <path d="m54 55h-48c-.552 0-1-.447-1-1v-15c0-.553.448-1 1-1h48c.552 0 1 .447 1 1v15c0 .553-.448 1-1 1zm-47-2h46v-13h-46z"/>
</svg>' . '<h4>E-shop na šabloně</h4>',
                        '<svg class="tab_img" height="511pt" viewBox="-2 0 511 511.99991" width="511pt" xmlns="http://www.w3.org/2000/svg">
    <path d="m436.550781 246.859375-17.933593 29.777344-59.566407-35.933594 17.953125-29.777344zm-214.171875 175.640625v-175.226562h69.546875v59.882812zm19.949219-244.773438h29.648437l15.453126 46.367188h-60.554688zm14.824219-81.136718c-6.402344 0-11.589844-5.191406-11.589844-11.589844 0-6.402344 5.1875-11.59375 11.589844-11.59375s11.589844 5.191406 11.589844 11.59375c0 6.398438-5.1875 11.589844-11.589844 11.589844zm0-96.589844 55.972656 74.625-39.953125 79.921875h-4.429687v-36.90625c18.109374-6.402344 27.597656-26.269531 21.195312-44.378906-6.402344-18.105469-26.269531-27.59375-44.375-21.191407-18.109375 6.402344-27.597656 26.269532-21.195312 44.375 3.5 9.902344 11.292968 17.691407 21.195312 21.195313v36.90625h-4.429688l-39.953124-79.921875zm-161.894532 179.363281-3.40625 2.246094-42.691406 28.121094-3.929687-32.847657c-.214844-1.785156-.84375-3.496093-1.832031-4.996093l-12.75-19.359375 19.359374-12.75 12.75 19.359375c.988282 1.496093 2.316407 2.746093 3.871094 3.648437zm-72.15625-80.457031 14.140626 21.480469-19.355469 12.75-17.386719-26.3125zm61.863282 114.972656 28.976562-19.136718 85.253906 129.324218v126.425782l-143.265624-217.460938zm201.964844 281.132813c-9.828126 16.503906-31.167969 21.914062-47.667969 12.085937-16.5-9.824218-21.910157-31.167968-12.085938-47.667968.070313-.117188.140625-.234376.210938-.347657l87.523437-145.15625h.199219v-.3125l31.96875-53.03125 29.777344 17.988281 29.824219 17.921876zm209.894531-303.199219c-5.980469 9.878906-14.164063 18.238281-23.914063 24.433594l-20.585937 13.039062-67.226563-40.476562 1.910156-24.339844c2.125-26.613281 18.332032-50.054688 42.484376-61.433594l74.183593-34.925781 1.46875 32.304687c-1.160156-.128906-2.332031-.078124-3.476562.152344l-28.074219 6.953125c-26.125 6.382813-46.355469 27.058594-52.160156 53.320313-1.394532 6.242187 2.53125 12.4375 8.773437 13.839844.835938.191406 1.6875.289062 2.539063.289062 5.425781-.003906 10.121094-3.769531 11.300781-9.0625 3.917969-17.660156 17.519531-31.5625 35.089844-35.863281l27.074219-6.714844 1.21875 26.660156c.679687 14.683594-3.011719 29.238281-10.605469 41.824219zm0 0"/>
</svg>' . '<h4>E-shop na míru</h4>'
                    ),
                    array(
                        $table1,
                        $table2
                    ), 'cenik_table');

                ?>

            </div>
        </div>
        <section id="banner_cesta" class="cesta_add"
                 style="background: url(<?php echo get_template_directory_uri() . "/images/cesta_k_zisku.jpg" ?>)">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <p>E-shop je důležitý první krok, ale cesta ke slušnému zisku je dlouhá.</p>
                            <p>Dostaňte své zboží mezi lidi a zvyšujte tržby pomocí online reklamních kanálů.</p>
                        </div>
                        <div class="col-md-3">
                            <a class="button_cesta" href="https://www.marketsoul.cz/project/sluzby-pro-e-shopy/"
                               target="_blank">Zjistit více</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="eshop_funkce text-center">
                        <?php echo $eshop_funkce ?>
                    </div>
                </div>
            </div>


            <!-- More Tables -->
            <!-- NAPOJENÍ -->
            <table id="napojeni" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cloud.png"/>
                            <p class="small-p"> Napojení</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25">
                        <div class="c_table_toggle ">
                            <button class="c_show_table" type="button" data-toggle="collapse"
                                    data-target="#collapseExample7" aria-expanded="false"
                                    aria-controls="collapseExample7">
                                <!--Zobrazit funkce-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!-- hidden table -->
            <div class="collapse colapse-adjustment" id="collapseExample7">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <!--Sociální sítě-->
                        <tr class="header_row">
                            <th scope="row" class="wid-25">Sociální sítě</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="wid-25">Facebook</th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Instagram</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Messenger</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Live Chat</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <!--Platení brány-->
                        <tr class="header_row">
                            <th scope="row" class="responsive-tr-5">Platební brány</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Comgate platební brána</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">GoPay</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Jiné - na míru
                                <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                            class="tooltip2text">Chybí ti nějaké napojení? Zkus najít plugin co to dokáže, nebo se nám ozvi a rádi ti ho přidáme.</span>
                                </div>
                            </th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>


                        <!--Doprava a Logistika-->
                        <tr class="header_row">
                            <th scope="row" class="responsive-tr-4">Doprava a Logistika</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Comgate logistika CZ</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Comgate logistika SK</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Zásilkovna</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Uloženka</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Jiné - na míru
                                <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                            class="tooltip2text">Chybí ti nějaké napojení? Zkus najít plugin co to dokáže, nebo se nám ozvi a rádi ti ho přidáme.</span>
                                </div>
                            </th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <!--Analytika-->
                        <tr class="header_row">
                            <th scope="row">Analytika</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Google Analytics</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-2">Google Analytics Enhanced Ecommerce</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Google Tag Manager</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <!--Marketing a reklamy-->
                        <tr class="header_row">
                            <th scope="row" class="responsive-tr-4">Marketing a reklamy</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-2">Napojení na vyhledávače cen</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center responsive-tr-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center responsive-tr-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <!--Emailing-->
                        <tr class="header_row">
                            <th scope="row">Emailing</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Mailchimp</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Smartemailing</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Jiné - na míru
                                <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                            class="tooltip2text">Chybí ti nějaké napojení? Zkus najít plugin co to dokáže, nebo se nám ozvi a rádi ti ho přidáme.</span>
                                </div>
                            </th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <!--Účetnictví a fakturace-->
                        <tr class="header_row">
                            <th scope="row" class="responsive-tr-4">Automatizované účetnictví a fakturace</th>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Vlastní manuální řešení fakturace</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Pohoda</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">iDoklad</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Fakturoid</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Jiné - na míru
                                <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i> <span
                                            class="tooltip2text">Chybí ti nějaké napojení? Zkus najít plugin co to dokáže, nebo se nám ozvi a rádi ti ho přidáme.</span>
                                </div>
                            </th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- More Tables -->
            <!-- E-commerce -->
            <table id="ecommerce" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25 wid-50">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart.png"/>
                            <p class="responsive-p-1"> E-commerce</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25 button-responsive">
                        <div class="c_table_toggle">
                            <button class="c_show_table " type="button" data-toggle="collapse"
                                    data-target="#collapseExample1" aria-expanded="false"
                                    aria-controls="collapseExample1">
                                <!--<span>Zobrazit funkce</span><b>Skrýt funkce</b>-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!-- Hidden Table-->
            <div class="collapse colapse-adjustment" id="collapseExample1">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="even_row">
                            <th scope="row" class="wid-25 responsive-tr-1">Přehledné statistiky eshopu - 2x dashboard
                            </th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-3">Cizí měny (€, Lb, USD)</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-1">Správa objednávek, zákazníků a produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Sklady s historií pohybů produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-2">Objednávkový proces (košík)</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Stavy objednávek</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-2">Pridávaní možností dopravy</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-3">Správa kategorií produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Výpočet DPH</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Export XML feedu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Import XML feedu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- More Tables -->
            <!-- PLATEBNÍ METODY -->
            <table id="platebni_metody" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25 wid-50">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/credit-card.png"/>
                            <p class="small-p responsive-p-2"> Platební metody</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25">
                        <div class="c_table_toggle">
                            <button class="c_show_table" type="button" data-toggle="collapse"
                                    data-target="#collapseExample2" aria-expanded="false"
                                    aria-controls="collapseExample2">
                                <!--Zobrazit funkce-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!-- Hidden Table-->
            <div class="collapse colapse-adjustment" id="collapseExample2">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="even_row">
                            <th scope="row" class="wid-25 responsive-tr-3">Hotově (při vyzvednutí)</th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Bankovní převod</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Dobírka</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Comgate</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">GoPay</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- More Tables -->
            <!-- DOPRAVNÍ METODY -->
            <table id="dopravni_metody" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25 wid-50">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/delivery.png"/>
                            <p class="small-p responsive-p-3"> Dopravní metody</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25">
                        <div class="c_table_toggle ">
                            <button class="c_show_table" type="button" data-toggle="collapse"
                                    data-target="#collapseExample3" aria-expanded="false"
                                    aria-controls="collapseExample3">
                                <!--Zobrazit funkce-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!-- Hidden Table-->
            <div class="collapse colapse-adjustment" id="collapseExample3">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="even_row">
                            <th scope="row" class="wid-25">Osobní oběr</th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Česká pošta</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">DPD</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">GLS</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">GEIS</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">PPL</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Zásílkovna</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row">Automatická doprava (při napojení na Comgate logistika)</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- More Tables -->
            <!-- PRODUKTY -->
            <table id="produkty" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/catalog.png"/>
                            <p class="small-p"> Produkty</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25">
                        <div class="c_table_toggle ">
                            <button class="c_show_table" type="button" data-toggle="collapse"
                                    data-target="#collapseExample4" aria-expanded="false"
                                    aria-controls="collapseExample4">
                                <!--Zobrazit funkce-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!-- Hidden Table-->
            <div class="collapse colapse-adjustment" id="collapseExample4">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="even_row">
                            <th scope="row" class="wid-25 responsive-tr-4">Počet produktů</th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-2">Vlastní dostupnost produktu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Skladové zásoby</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Historie skladu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-3">Export produktů v Excelu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="small-tr">Nastavení samostatní ceny pro každou měnu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-2">Slevy - číselně i procentuálně</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Slevové kupony</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-1">Export zákazníků a objednávek v Exceli</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-2">Graf vývoje prodeje produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-3">Související produkty</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-3">Informační štítky produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-3">Vyhledávaní produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-2">Řazení a filtrace produktů</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row">Fotogalerie</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="small-tr">Varianty produktů s rovnakou cenou (barva, velikost)</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="small-tr">Varianty produktů s rozdílnou cenou (počet ks v balení, a
                                pod.)
                            </th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- More Tables -->
            <!-- OBJEDNAVKY -->
            <table id="objednavky" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/shopping.png"/>
                            <p class="small-p"> Objednávky</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25"></th>
                    <th scope="col" class="wid-25">
                        <div class="c_table_toggle ">
                            <button class="c_show_table" type="button" data-toggle="collapse"
                                    data-target="#collapseExample5" aria-expanded="false"
                                    aria-controls="collapseExample5">
                                <!--Zobrazit funkce-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!-- Hidden Table-->
            <div class="collapse colapse-adjustment" id="collapseExample5">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="even_row">
                            <th scope="row" class="wid-25 responsive-tr-2">Editace a správa objednávek</th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-3">Řazení a filtrace objednávek</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-3">Export objednávek v Excelu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="small-tr">Automatický přepis stavů z Comgate (při napojení na Comgate
                                logistika)
                            </th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-1">Vlastní nastavení možností dopravy a platby</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-3">Nastavitelne ceny dopravy</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-2">Generování a správa faktur (PDF)</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-3">Automatické emaily (dle stavu objednávky)</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ZAKAZNICI -->
            <!-- More Tables -->

            <table id="zakaznici" class="table more-tables">
                <thead>
                <tr class="header_row">
                    <th scope="col" class="wid-25">
                        <div class="header_more_tables">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/multiple-users.png"/>
                            <p class="small-p"> Zákazníci</p>
                        </div>
                    </th>
                    <th scope="col" class="wid-25">

                    </th>
                    <th scope="col" class="wid-25">

                    </th>
                    <th scope="col" class="wid-25">
                        <div class="c_table_toggle ">
                            <button class="c_show_table" type="button" data-toggle="collapse"
                                    data-target="#collapseExample6" aria-expanded="false"
                                    aria-controls="collapseExample6">
                                <!--Zobrazit funkce-->
                            </button>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!-- hidden table -->
            <div class="collapse colapse-adjustment" id="collapseExample6">
                <div class="card card-body card-adjustment">
                    <table class="table more-tables2">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="even_row">
                            <th scope="row" class="wid-25 responsive-tr-4">Editace a správa zákazníků</th>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center wid-25"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-4">Řazení a filtrace zákazníků</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="even_row">
                            <th scope="row" class="responsive-tr-4">Export zákazníků v Excelu</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        <tr class="odd_row">
                            <th scope="row" class="responsive-tr-2">Historie objednávek pro zákazníků</th>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            <td class="text-center"><img
                                        src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </section>

    <!-- Pre Last part -->
    <section id="c_prelast_part">
        <div class="container" id="c_prelast_container">
            <div class="row c_prelast_row">
                <div class="col-lg-7 col-md-12">
                    <div class="c_agentura">
                        Pokud jste <b>agentura</b> nebo <b>freelancer</b> a máte o plugin zájem, dejte nám vědet.
                    </div>
                </div>
                <div class="col-lg-2">

                </div>
                <div class="col-lg-3 col-md-12 pl-4">
                    <div class="text-center">
                        <div class="c_agentura_button">
                            <div class="c_new_button">
                                <a href="<?php echo get_page_link(16); ?>" class="new_button">
                                    Kontaktovat
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?php
}
wp_reset_query();
?>


<?php get_footer(); ?>