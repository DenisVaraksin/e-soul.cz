<?php
/*
Template Name: data protection
Template Post Type: page
*/
get_header();?>

<!-- Mandatory Info -->
	<section id="data_over_firstpart">
		<div class="container data_content">
			<div class="row">
				<div class="col">
					<div class="data_form_title text-center">
						Zásady ochrany osobních údajů
					</div>

					<div>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
							the_content();
						endwhile; ?>
						<?php endif; ?>
					</div>

				</div>
			</div>
		</div>
	</section>


<?php get_footer(); ?>