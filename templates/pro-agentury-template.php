<?php
/*
Template Name: Pro agentury
Template Post Type: page
*/
get_header(); ?>

<!-- Slider + Co Je Esoul (2)-->

<?php
$args = array(
    'post_type' => array('pro_agentury'), // post type we display
    'order' => 'DESC',
    'posts_per_page' => 1,
    "orderby" => "menu_order",
    "post_status" => "publish",
);
?>
<?php
$loop3 = new WP_Query($args);
while ($loop3->have_posts()) {
    $loop3->the_post();
    ?>


    <section id="aa_over_firstpart">


        <div class="container" id="fourth_container">
            <div class="row">
                <div class="col aa_what_esoul">
                    <?php
                    $aa_esoul_title = get_field('aa_what_esoul_title');
                    $aa_esoul_subtitle = get_field('aa_esoul_subtitle');
                    $aa_esoul_text = get_field('aa_esoul_text');
                    ?>
                    <div class="aa_esoul_title text-center">
                        <?php echo $aa_esoul_title; ?>
                    </div>
                    <div class="aa_esoul_subtitle text-center">
                        <?php echo $aa_esoul_subtitle; ?>
                    </div>
                    <div class="aa_esoul_text text-center">
                        <?php echo $aa_esoul_text; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    $image = get_field('infobox_image', 46);
    $images_boxes = get_field('infobox_images_boxes', 46);
    ?>

    <section class="info_box" id="hp_infobox">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="info_box_images" id="infoleftbox">
                        <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>"
                             class="image_fade_left animated"/>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 text-right" id="inforightbox">
                    <h2 class="esoul_title text-white"><?php echo get_field('infobox_title', 46) ?></h2>
                    <p class="esoul_text text-white"><?php echo get_field('infobox_description', 46) ?></p>
                    <ul class="info_box_img_list">
                        <?php
                        foreach ($images_boxes as $key => $image) {
                            ?>
                            <li><a href="<?php echo $image['url']; ?>"
                                   data-lightbox="infobox_image"><?php echo($key == 0 ? 'Zobrazit ukázku (galerie)' : null) ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="porovnani">
        <div class="container">
            <h2><?php echo get_field("nadpis_porovnani"); ?></h2>
            <h3><?php echo get_field("podnadpis_porovnani"); ?></h3>
            <div class="row">
                <div class="col-md-12">
                    <?php echo get_field("text_porovnani"); ?>
                    <div class="overflow_table">

                        <table class="table" id="c_first_table">
                            <thead>
                            <tr class="header_row_bubble">
                                <th scope="col" id="table_first_row" class="wid-25">
                                </th>
                                <th scope="col" class="wid-25" id="c_first_th">
                                    <div class="c_first_block">
                                        <div><img class="logo_table"
                                                  src="<?php echo get_stylesheet_directory_uri() ?>/img/logo_agentura.png"/>
                                        </div>
                                    </div>
                                </th>
                                <th scope="col" class="wid-25" id="c_second_th">
                                    <div class="c_second_block">
                                        <div><img class="logo_table"
                                                  src="<?php echo get_stylesheet_directory_uri() ?>/img/logo_woo.png"/>
                                        </div>
                                    </div>
                                </th>
                                <th scope="col" class="wid-25" id="c_second_th">
                                    <div class="c_second_block">
                                        <div><img class="logo_table"
                                                  src="<?php echo get_stylesheet_directory_uri() ?>/img/shoptet_logo.png"/>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr class="even_row">
                                <td scope="row">CMS WordPress
                                    <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i><span
                                                class="tooltip2text">V průměru každý 3. web na světě beží na CMS WordPress, proto jsme si jako základ našeho pluginu zvolili právě tento redakční systém</span>
                                    </div>
                                </td>
                                <td class="text-center responsive-tr-3"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center">Vlastní CMS</td>
                            </tr>
                            <tr class="odd_row">
                                <td scope="row" class="responsive-tr-4">Grafický design</td>
                                <td class="text-center">Design na míru</td>
                                <td class="text-center">Šablony (většinou placené)</td>
                                <td class="text-center">9 prostých šablon</td>
                            </tr>
                            <tr class="even_row">
                                <td scope="row">Administrace e-shopu
                                    <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i><span
                                                class="tooltip2text">Naše administrace e-shopu je jednoduchá, přehledná a s webem tvoří jeden celek</span>
                                    </div>
                                </td>
                                <td class="text-center">9/10</td>
                                <td class="text-center">4/10</td>
                                <td class="text-center">7/10</td>
                            </tr>
                            <tr class="odd_row">
                                <td scope="row" class="responsive-tr-4">Lokalizace na CZ/SK trh
                                    <div class="tooltip2"><i class="fas info_ico fa-info-circle"></i><span
                                                class="tooltip2text">Během několika minut nastavíte možnosti fakturace, dopravy i jiné napojení na nejpoužívanější české služby</span>
                                    </div>
                                </td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            </tr>
                            <tr class="even_row">
                                <td scope="row">Rozšíření o WP pluginy ZDARMA</td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                            </tr>
                            <tr class="odd_row">
                                <td scope="row" class="responsive-tr-4">Cizí měny</td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center">V rámci rozšíření</td>
                                <td class="text-center">V rámci placeného rozšíření</td>
                            </tr>
                            <tr class="even_row">
                                <td scope="row" class="responsive-tr-2">Integrovaná fakturace</td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            </tr>
                            <tr class="odd_row">
                                <td scope="row" class="responsive-tr-4">Integrovaná doprava</td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            </tr>
                            <tr class="even_row">
                                <td scope="row">Integrované platební brány</td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                                <td class="text-center"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/cancel.png"/></td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            </tr>
                            <tr class="even_row">
                                <td scope="row">XML feed produktů</td>
                                <td class="text-center">V rámci rozšíření</td>
                                <td class="text-center">V rámci rozšíření</td>
                                <td class="text-center responsive-tr-2"><img
                                            src="<?php echo get_stylesheet_directory_uri() ?>/img/tick2.png"/></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <a href="<?php echo get_site_url(); ?>/kontakt" class="button_agentura">Kontaktujte nás</a>
        </div>
    </section>

    <section id="aa_over_thirdpart" style="background:url(<?php echo get_template_directory_uri() . "/img/green_background.png" ?>) no-repeat; ">
        <div class="container">
            <div class="row">
                <?php
                $price_pre_title = get_field('price_pre_title');
                $price_title = get_field('price_title');
                $price_subtitle = get_field('price_subtitle');
                ?>
                <div class="col-lg-8 col-md-13">
                    <div class="aa_price_first">
                        <?php echo $price_pre_title ?>
                    </div>
                    <div class="aa_price_second">
                        <?php echo $price_title ?>
                    </div>
                    <div class="aa_price_third">
                        <?php echo $price_subtitle ?>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-3">
                    <a href="<?php echo get_page_link(16); ?>" class="aa_contact_button text-center">
                        KONTAKT
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- About plugin -->
    <section id="aa_over_secondpart">
        <div class="first_arrow">
            <img id="arrow1" src="<?php echo get_stylesheet_directory_uri() ?>/img/arrow1.png"/>
        </div>
        <div class="second_arrow">
            <img id="arrow2" src="<?php echo get_stylesheet_directory_uri() ?>/img/arrow2.png"/>
        </div>
        <div class="container" id="aa_secondcontainer">
            <div class="row">
                <?php
                $aa_plugin_title = get_field('aa_plugin');
                ?>
                <div class="col">
                    <div class="aa_plugin_title text-center">
                        <?php echo $aa_plugin_title; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if (have_rows('plugin_first')) :
                    while (have_rows('plugin_first')) : the_row();
                        $title = get_sub_field('title');
                        $subtitle = get_sub_field('subtitle');
                        $text = get_sub_field('text');
                        $picture = get_sub_field('picture');
                        ?>
                        <div class="col-md-4 offset-md-1">
                            <img src="<?php echo $picture; ?>"/>
                        </div>
                        <div class="col-md-5 offset-md-1">
                            <div class="plugin_title">
                                <?php echo $title; ?>
                            </div>
                            <div class="plugin_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="plugin_text">
                                <?php echo $text; ?>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>

            <div class="row pt-5">
                <?php
                if (have_rows('plugin_second')) :
                    while (have_rows('plugin_second')) : the_row();
                        $title = get_sub_field('title');
                        $subtitle = get_sub_field('subtitle');
                        $text = get_sub_field('text');
                        $picture = get_sub_field('picture');
                        ?>
                        <div class="col-md-4 offset-md-1 order-md-last">
                            <img src="<?php echo $picture; ?>"/>
                        </div>
                        <div class="col-md-5 offset-md-1 order-md-first">
                            <div class="plugin_title">
                                <?php echo $title; ?>
                            </div>
                            <div class="plugin_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="plugin_text">
                                <?php echo $text; ?>
                            </div>
                        </div>
                        <div class="col-md-1 order-md-last"></div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>

            <div class="row pt-5">
                <?php
                if (have_rows('plugin_third')) :
                    while (have_rows('plugin_third')) : the_row();
                        $title = get_sub_field('title');
                        $subtitle = get_sub_field('subtitle');
                        $text = get_sub_field('text');
                        $picture = get_sub_field('picture');
                        ?>
                        <div class="col-md-4 offset-md-1">
                            <img src="<?php echo $picture; ?>"/>
                        </div>
                        <div class="col-md-5 offset-md-1">
                            <div class="plugin_title">
                                <?php echo $title; ?>
                            </div>
                            <div class="plugin_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="plugin_text">
                                <?php echo $text; ?>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>

            <div class="row pt-5">
                <?php
                if (have_rows('plugin_fourth')) :
                    while (have_rows('plugin_fourth')) : the_row();
                        $title = get_sub_field('title');
                        $subtitle = get_sub_field('subtitle');
                        $text = get_sub_field('text');
                        $picture = get_sub_field('picture');
                        ?>
                        <div class="col-md-4 offset-md-1 order-md-last">
                            <img src="<?php echo $picture; ?>"/>
                        </div>
                        <div class="col-md-5 offset-md-1 order-md-first">
                            <div class="plugin_title">
                                <?php echo $title; ?>
                            </div>
                            <div class="plugin_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="plugin_text">
                                <?php echo $text; ?>
                            </div>
                        </div>
                        <div class="col-md-1 order-md-last"></div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>

        </div>
    </section>

    <!-- PRICE (hidden part)-->

    <?php
}
wp_reset_query();
?>

<?php get_footer(); ?>
