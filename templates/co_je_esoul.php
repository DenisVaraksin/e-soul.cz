<?php
/*
Template Name: Co je E-soul
Template Post Type: page
*/
get_header();?>

<!-- Slider + Co Je Esoul (2)-->

<?php
$args = array(
    'post_type' => array( 'pro_agentury' ), // post type we display
    'order' => 'DESC',
    'posts_per_page' => 1,
    "orderby" => "menu_order",
    "post_status" => "publish",
);
?>
<?php
$loop3 = new WP_Query($args);
while ($loop3->have_posts()) {
    $loop3->the_post();
    ?>


    <section id="aa_over_firstpart">



        <div class="container" id="fourth_container">
            <div class="row">
                <div class="col aa_what_esoul">
                    <?php
                    $aa_esoul_title = get_field('esoul_jumbotron', 357)['esoul_title'];
                    $aa_esoul_subtitle = get_field('esoul_jumbotron', 357)['esoul_subtitle'];
                    $aa_esoul_text = get_field('esoul_jumbotron', 357)['esoul_text'];
                    ?>
                    <div class="aa_esoul_title text-center">
                        <?php echo $aa_esoul_title; ?>
                    </div>
                    <div class="aa_esoul_subtitle text-center">
                        <?php echo $aa_esoul_subtitle; ?>
                    </div>
                    <div class="aa_esoul_text text-center">
                        <?php echo $aa_esoul_text; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="second_part">
        <div class="container">
            <h2><?php echo get_field("nadpis_balicek")?></h2>
            <div class="row list_package">

                <?php

                // check if the repeater field has rows of data
                if( have_rows('balicky') ):

                    // loop through the rows of data
                    while ( have_rows('balicky') ) : the_row();

                        // display a sub field value
                        $ikonka = get_sub_field('ikonka');
                        $nadpis = get_sub_field('nadpis');
                        $text = get_sub_field('text');
                        ?>
                        <div class="col-md-4 single-package">
                            <div class="circle flex">
                                <img src="<?php echo $ikonka;?>">
                            </div>
                            <h3><?php echo $nadpis;?></h3>

                            <?php echo $text;?>


                        </div>
                    <?php
                    endwhile;

                else :

                    // no rows found

                endif;

                ?>

            </div>
            <a href="https://msesoul.marketsoul.cz/" target="_blank" class="button_agentura">Vyzkoušet demo</a>
        </div>
    </section>

    <section id="third_part">

        <div class="container">
            <h2><?php echo get_field("nadpis_cas")?></h2>
            <div class="row">


                <?php

                // check if the repeater field has rows of data
                if( have_rows('rada') ):
                    $ia = 1;
                    // loop through the rows of data
                    while ( have_rows('rada') ) : the_row();

                        // display a sub field value
                        $ikonka = get_sub_field('ikonka');
                        $nadpis = get_sub_field('nadpis');
                        $text = get_sub_field('text');
                        ?>


                        <div class="col-md-12 single-row-third">
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="icon_line"><img src="<?php echo $ikonka;?>"></div>

                                </div>
                                <div class="col-md-5">
                                    <h3><?php echo $nadpis;?></h3>

                                    <?php echo $text;?>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">


                                        <?php

                                        // check if the repeater field has rows of data
                                        if( have_rows('obrazky') ):

                                            // loop through the rows of data
                                            while ( have_rows('obrazky') ) : the_row();

                                                // display a sub field value
                                                $miniatura = get_sub_field('miniatura');
                                                $obrazek = get_sub_field('obrazek');
                                                $podnadpis = get_sub_field('podnadpis');
                                                $neni_obrazek = get_sub_field('neni_obrazek');
                                                $neni_obrazek_ico = get_sub_field('neni_obrazek_ico');

                                                if(!empty($neni_obrazek)){
                                                    ?>
                                                    <div class="col-md-6 single-admin-image no_image">
                                                        <img src="<?php echo $neni_obrazek_ico;?>">
                                                        <?php echo $neni_obrazek;?>
                                                    </div>
                                                    <?php
                                                }else{
                                                    ?>
                                                    <div class="col-md-6 single-admin-image">
                                                        <a href="<?php echo $obrazek;?>" data-lightbox="image-<?php echo $ia;?>">
                                                            <div class="admin_img flex" style="background: url(<?php echo $miniatura;?>)">
                                                                <div class="lupa flex">
                                                                    <img src="<?php echo get_template_directory_uri() . "/img/magnifying-glass2.svg"?>">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="title_photo"><?php echo $podnadpis;?></div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                $ia++;
                                            endwhile;

                                        else :

                                            // no rows found

                                        endif;

                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>



                    <?php
                    endwhile;
                    $ia++;
                else :

                    // no rows found

                endif;

                ?>

            </div>
        </div>

    </section>

    <?php
}
wp_reset_query();
?>

<?php get_footer(); ?>
