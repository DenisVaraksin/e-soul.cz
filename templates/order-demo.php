<?php
/*
Template Name: VYZKOUŠEJ DEMO
Template Post Type: page
*/
get_header();?>

<!-- Contact Form -->

<?php
$args = array(
	'post_type' => array( 'order_demo' ), // post type we display
	'order' => 'DESC',
	'posts_per_page' => 1,
	"orderby" => "menu_order",
	"post_status" => "publish",
);
$loop3 = new WP_Query($args);
while ($loop3->have_posts()) {
$loop3->the_post();
?>

<section id="od_over_firstpart">
	<div class="container">
		<div class="row">
			<?php
			$form_title = get_field('form_title');
			$phone_number = get_field('phone_number')
			?>
			<div class="col">
				<div class="od_form_title text-center">

                    <?php echo $form_title; ?>
				</div>
			</div>
		</div>
		<div class="row">

            <div class="col-md-5 offset-md-1 order-md-last">
                <div class="podpora" id="podpora-id">
                        <div class="podpora-picture">
                            <img src="<?php echo get_stylesheet_directory_uri()?>/img/mario_photo.jpg" />
                        </div>
                        <div class="podpora-icon">
                            <img src="<?php echo get_stylesheet_directory_uri()?>/img/phone.png" />
                        </div>
                        <div class="podpora-content">
                            <div class="podpora-first-text">
                                podpora
                            </div>
                            <div class="podpora-second-text">
		                        <?php echo $phone_number; ?>
                            </div>
                        </div>

                </div>
                <div class="form_contact">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/img/form_contact.png"/>
                </div>
            </div>
			<div class="col-md-6 order-md-first form-container ">

                <div class="c_background"></div>

				<div class="c_shortcode">
					
					
					<h2 class="contact-title-custom"><?php echo get_field("title_form_top");?></h2>
					<div class="contact-text-custom"><?php echo get_field("text_form");?></div>
					<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?></div>
			</div>
		</div>
	</div>
</section>



	<!-- About us -->
<section id="mapa_kontakt">
		<div class="container info_kontakt">
			<div class="row">
				<div class="col">
					<div class="title_contact text-center">
						Kontakt
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 kancelare">
					<?php echo get_field("kancelare")?>	
				</div>
				<div class="col-md-6 fakturacni_udaje">
					<?php echo get_field("fakturacni_udaje")?>	
				</div>	
			</div>
		</div>
		<div class="container-fluid">
			<?php 
			$location = get_field('mapa');
			?>
            <div class="acf-map" data-zoom="16">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"><p class="address"><?php echo $location['address']; ?></p></div>
            </div>
		</div>
</section>

	<!-- About us -->
<section id="od_over_secondpart">
		<div class="container">
			<div class="row">
				<?php
				$about_title = get_field('about_us_title');
				$about_text = get_field('about_us_text');
				?>
				<div class="col">
					<div class="od_about_title text-center">
						<?php echo $about_title; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="od_vizitka">
						<img id="vizitka" src="<?php echo get_stylesheet_directory_uri() ?>/img/vizitka.png"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="od_about_logo">
						<img id="about_logo" src="<?php echo get_stylesheet_directory_uri() ?>/img/about_logo.png"/>
					</div>
					<div class="od_about_text">
						<?php
						if (have_rows('about_us_text')) :
							while (have_rows('about_us_text')) :
								the_row();
								$first = get_sub_field('first_part');
								$second = get_sub_field('bold_part');
								$third = get_sub_field('third_part');
								$fourth = get_sub_field('fourth_part');
								?>
								<div class="od_about_firsttext">
									<?php echo $first; ?><b><?php echo $second; ?></b> <?php echo $third?>
								</div>
								<div class="od_about_secondtext">
									<?php echo $fourth?>
								</div>
							<?php
							endwhile;
						endif;
						?>
					</div>
				</div>
                <div class="col-md-12">
                    <div class="marketsoul-link mt-4">
                        <a href="https://www.marketsoul.cz/" target="_blank">www.marketsoul.cz</a>
                    </div>
                </div>
			</div>
		</div>
</section>



<?php
}
wp_reset_query();
?>

<?php get_footer(); ?>
