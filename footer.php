<section id="over_eight_part">
    <div class="footer_city"
         style="background:url(<?php echo get_template_directory_uri() . "/img/krajina-2.png" ?>) ">
    </div>
    <div class="footer_city_mobile"
         style="background:url(<?php echo get_template_directory_uri() . "/img/krajina-2.png" ?>) no-repeat;">
    </div>

    <div class="container" id="eight_container">
        <div class="row">
            <div class="col">
                <div class="order_demo_title text-center">
                    Neváhej a vyzkoušej si demo ZDARMA
                </div>
                <div class="text-center">
                    <a href="https://msesoul.marketsoul.cz/" target="_blank" class="order_demo_button">
                        <span class="d-inline-block order-button-child">VYZKOUŠEJ DEMO</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer" role="contentinfo">
    <div class="container" id="footer_container">
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <div class="footer_company">
                    © 2019 MarketSoul s.r.o.
                </div>
                <div class="footer_description">
                    E-commerce řešení na míru pro váš e-shop
                </div>
                <?php echo wp_nav_menu(array('menu' => 3, 'menu_class' => 'footer_links')); ?>
            </div>
            <div class="col-md-1"></div>
            <div class="col-sm-12 col-md-3 ">
                <div class="footer_contacts">
                    <div class="footer_phone">
                        <span>Telefon: <a href="tel:770167897">+420 770 167 897</a></span>
                    </div>
                    <div class="footer_email">
                        <span>E-mail:</span> <u><a href="mailto:info@e-soul.cz" target="_blank"
                                                   rel="noopener noreferrer">info@e-soul.cz</a></u>
                    </div>
                </div>
                <div class="footer_icons ">
                    <div class="footer_fb d-inline-block">
                        <a href="https://www.facebook.com/eshopbezsablon" target="_blank"><img
                                    src="<?php echo get_stylesheet_directory_uri() ?>/img/fb_icon.png"/></a>
                    </div>
                    <div class="footer_li d-inline-block">
                        <a href="https://ru.linkedin.com/company/marketsoul-s.r.o." target="_blank"><img
                                    src="<?php echo get_stylesheet_directory_uri() ?>/img/in_icon.png"/></a>
                    </div>
                    <div class="footer_inst d-inline-block">
                        <a href="https://www.instagram.com/marketsoul/" target="_blank"><img
                                    src="<?php echo get_stylesheet_directory_uri() ?>/img/inst_icon.png"/></a>
                    </div>
                </div>
            </div>

            <div class="col-md-0"></div>
            <div class="col-sm-12 col-md-3">
                <div class="footer-made-by-wrapper">
                    <div class="footer-made-by">
                        Vyrobila digitální agentura
                    </div>
                    <div class="footer-made-by-img">
                        <a href="https://www.marketsoul.cz/" target="_blank"><img
                                    src="<?php echo get_stylesheet_directory_uri() ?>/img/grey-logo3.png"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php edit_post_link(); ?>
<?php wp_footer(); ?>
</body>
</html>
