<?php get_header(); ?>

<!-- Play button and header1 -->
<script>
    var names = [' unikátní', ' jedinečný', ' originální', ' svůj', ' mimořádný'];

    setInterval(function () {
        var rand = Math.floor(Math.random() * 5);
        document.getElementById("name").innerHTML = names[rand];
    }, 2500);
</script>

<!-- First Section -->
<section id="over_first_part">

    <div class="header_city exclude_lazyload"
         style="background:url(<?php echo get_template_directory_uri() . "/img/city.svg" ?>) no-repeat;"></div>
    <div id="moving_people">
        <img id="moving_people_img" src="<?php echo get_stylesheet_directory_uri() ?>/img/people.png" class="exclude_lazyload"/>
    </div>
    <div id="play_container" class="container nowrap">
        <div class="row">
            <div class="col">
                <h1 class="play_header"><p>Buď </p>
                    <p id="name"> originální</p></h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h2 class="play_miniheader">Řekni ne šabloně a vytvoř si <p> e-shop na míru</p></h2>
            </div>
        </div>
        <div class="row" id="play_button">
            <div class="col">
                <a href="#esoul_nav_video" id="esoul_nav_video_link">
                    <!--
                    <div id="play_button_img">
                        <img id="play_image"
                             src="<?php // echo get_stylesheet_directory_uri() ?>/img/play_button_white.png"/>
                    </div>
                    -->
                </a>
            </div>
        </div>
        <div id="moving_people_mobile">
            <img id="moving_people_img_mobile" src="<?php echo get_stylesheet_directory_uri() ?>/img/people.png" class="exclude_lazyload"/>
        </div>
    </div>
</section>


<!-- Advatages Posts Display -->
<?php
$args = array(
    'post_type' => array('box_pluses'), // post type we display
    'order' => 'DESC',
    'posts_per_page' => 4,
    "orderby" => "menu_order",
    "post_status" => "publish",
);
?>

<div id="plus_box" class="container">
    <div class="row">
        <?php
        $i = 0;
        $loop = new WP_Query($args);
        while ($loop->have_posts()) {
            $loop->the_post();
            $i = $i + 1;
            $picture = get_field("post_picture");
            $text = get_field("post_text");
            $title = get_field("post_title");
            ?>
            <div class="col-12 col-sm-6 col-lg-3 p-unset">
                <div class="plus_box_hover">
                    <div class="plus_box_one" id="plus_box-<?php echo $i ?>">
                        <img class="exclude_lazyload" src="<?php echo $picture; ?>"/>
                        <div class="plus_box_title">
                            <?php echo $title; ?>
                        </div>
                        <div class="plus_box_text">
                            <?php echo $text; ?>
                        </div>
                    </div>

                </div>
            </div>
            <?php
        }
        wp_reset_query();
        ?>
    </div>
</div>

<!-- What is actually E-soul (Co Je Esoul) -->

<?php
$args = array(
    'post_type' => array('what_esoul'), // post type we display
    'order' => 'DESC',
    'posts_per_page' => 1,
    "orderby" => "menu_order",
    "post_status" => "publish",
);
?>

<section id="over_second_part"
         style="background:url(<?php echo get_template_directory_uri() . "/img/secondsection.png" ?>) no-repeat; background-size: contain;">

    <div class="container" id="second_container">

        <div class="row">
            <?php

            $loop2 = new WP_Query($args);
            while ($loop2->have_posts()) {
            $loop2->the_post();

            $title2 = get_field("esoul_title");
            $subtitle2 = get_field("esoul_subtitle");
            $text2 = get_field("esoul_text");
            ?>

            <div class="col">

                <div class="esoul_title text-center" id="co_je_esoul">
                    <?php echo $title2; ?>
                </div>
                <div class="esoul_subtitle text-center">
                    <?php echo $subtitle2; ?>
                </div>
                <div class="esoul_text text-center text_custom_style">
                    <?php echo $text2; ?>
                </div>
                <div class="esoul_icons" id="esoul_js_test">
                    <div class="icon_1" data-aos="fade-up-left" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/first_icon.png" ?>">
                    </div>
                    <div class="icon_2" data-aos="fade-left" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/second_icon.png" ?>">
                    </div>
                    <div class="icon_3" data-aos="fade-down-left" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/third_icon.png" ?>">
                    </div>
                    <div class="icon_logo" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/logo_big.png" ?>">
                    </div>
                    <div class="icon_4" data-aos="fade-up-right" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/fourth_icon.png" ?>">
                    </div>
                    <div class="icon_5" data-aos="fade-right" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/fifth_icon.png" ?>">
                    </div>
                    <div class="icon_6" data-aos="fade-down-right" data-aos-duration="1200">
                        <img src="<?php echo get_template_directory_uri() . "/img/sixth_icon.png" ?>">
                    </div>
                </div>
                <!-- for smaller screens -->
                <div class="esoul_icons_mobile pb-4">
                    <img src="<?php echo get_template_directory_uri() . "/img/co_je_esoul_mobile.png" ?>">
                </div>
            </div>

        </div>
    </div>
</section>

<!-- VIDEO -->
<!--
<section id="over_third_part"
         style="background:url(<?php echo get_template_directory_uri() . "/img/thirdsection.png" ?>) no-repeat; background-size: contain;">
     <div class="container" id="third_container">
         <?php $nav_title = get_field("esoul_navigation");
$nav_link = get_field("esoul_nav_link"); ?>

         <div class="row">
             <div class="col">
                 <div class="esoul_nav text-center" id="esoul_nav_video">
	                <?php// echo $nav_title; ?>
                </div>
                <div class="esoul_play">
                    <a id="link_size">
                        <div id="play_button_img_down">
                            <img src="<?php// echo get_stylesheet_directory_uri() ?>/img/play_button_white.png"/>
                        </div>
                    </a>
                </div>
             </div>
         </div>
     </div>
</section>
-->
<!-- The Modal Video Popup HIDDEN BY DEFAULT -->
<!--
    <div id="myModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="viedo_block">
                <iframe class="embed-responsive-item" style="width: 100%; height: 480px" src="<?php// echo $nav_link;?>?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
-->
<!-- Slider -->
<?php
$image = get_field('infobox_image');
$images_boxes = get_field('infobox_images_boxes');
?>
    <section class="info_box" id="sablona">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="info_box_images" id="infoleftbox">
                        <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>"
                             class="image_fade_left animated"/>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 text-right" id="inforightbox">
                    <h2 class="esoul_title text-white"><?php echo get_field('infobox_title') ?></h2>
                    <p class="esoul_text text-white"><?php echo get_field('infobox_description') ?></p>
                    <ul class="info_box_img_list">
                        <?php
                        foreach ($images_boxes as $key => $image) {
                            ?>
                            <li><a href="<?php echo $image['url']; ?>"
                                   data-lightbox="infobox_image"><?php echo($key == 0 ? 'Zobrazit ukázku (galerie)' : null) ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="over_fourth_part">
        <div class="container" id="fourth_container">
            <?php $slider_title = get_field("slider_title"); ?>
            <div class="row">
                <div class="col">
                    <div class="slider_title text-center">
                        <?php echo $slider_title ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col d-flex">
                    <div id="slider" class="m-auto">
                        <?php $i = 1;
                        if (have_rows('esoul_slider')) :?>
                            <?php while (have_rows('esoul_slider')) : the_row();
                                $inside_slider_title = get_sub_field('inside_slider_title');
                                $inside_slider_text = get_sub_field('inside_slider_text');
                                $inside_slider_picture = get_sub_field('inside_slider_picture');
                                ?>
                                <div class="slides">
                                    <div class="slide d-flex <?php if ($i == 1) echo "active"; ?>"
                                         id="<?php echo $i ?>">
                                        <div class="inside_slider_picture">
                                            <img src="<?php echo $inside_slider_picture; ?>"/>
                                        </div>
                                        <div class="inside_slider_content d-block">
                                            <div class="inside_slider_title pb-3">
                                                <?php echo $inside_slider_title; ?>
                                            </div>
                                            <div class="inside_slider_text">
                                                <?php echo $inside_slider_text; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $i = $i + 1;
                            endwhile;
                        endif;
                        ?>
                    </div>
                    <div id="slickRight_main_home">
                        <button>></button>
                    </div>

                </div>
            </div>
            <div class="row s-mobile-row">
                <div class="col">
                    <div class="order_button_adj">
                        <a id="order-button" class="d-inline-block" href="https://msesoul.marketsoul.cz/" target="_blank">
                            <p class="d-inline-block order-button-child">VYZKOUŠEJ DEMO</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>


<!-- Connections (Napojeni) -->
<!-- First Part-->
    <section id="over_fifth_part">
        <div class="container" id="fourth_container">
            <?php $connections_title = get_field("connections_title"); ?>
            <div class="row">
                <div class="col">
                    <div class="connections_title text-center">
                        <?php echo $connections_title; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php
                if (have_rows('connections_subtitle_first')) :
                    while (have_rows('connections_subtitle_first')) : the_row();
                        $subtitle = get_sub_field('subtitle');
                        $picture_first = get_sub_field('picture_first');
                        $picture_second = get_sub_field('picture_second');
                        ?>

                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="connections_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="connections_logos d-flex">
                                <div class="connections_logo_first ">
                                    <img src="<?php echo $picture_first; ?>"/>
                                </div>
                                <div class="connections_logo_second ">
                                    <img src="<?php echo $picture_second; ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>

                <?php
                if (have_rows('connections_subtitle_second')) :
                    while (have_rows('connections_subtitle_second')) : the_row();
                        $subtitle = get_sub_field('subtitle');
                        $picture_first = get_sub_field('picture_first');
                        $picture_second = get_sub_field('picture_second');
                        ?>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="connections_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="connections_logos d-flex">
                                <div class="connections_logo_first">
                                    <img src="<?php echo $picture_first; ?>"/>
                                </div>
                                <div class="connections_logo_second">
                                    <img src="<?php echo $picture_second; ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
                <?php
                if (have_rows('connections_subtitle_third')) :
                    while (have_rows('connections_subtitle_third')) : the_row();
                        $subtitle = get_sub_field('subtitle');
                        $picture_first = get_sub_field('picture_first');
                        ?>
                        <div class="col-6 col-sm-3 col-md-2 pt-4">
                            <div class="connections_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="connections_logo_first pt-3 mobile-padding">
                                <img src="<?php echo $picture_first; ?>"/>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
                <?php
                if (have_rows('connections_subtitle_fourth')) :
                    while (have_rows('connections_subtitle_fourth')) : the_row();
                        $subtitle = get_sub_field('subtitle');
                        $picture_first = get_sub_field('picture_first');
                        ?>
                        <div class="col-6 col-sm-3 col-md-2 pt-4">
                            <div class="connections_subtitle">
                                <?php echo $subtitle; ?>
                            </div>
                            <div class="connections_logo_first pt-3 ">
                                <img src="<?php echo $picture_first; ?>"/>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>

            <!-- Second Part-->
            <div class="row pt-5">
                <?php $connections_subtitle = get_field("connections_subtitle"); ?>
                <div class="col text-center">
                    <div class="connections_subtitle_second ">
                        <?php echo $connections_subtitle ?>
                    </div>
                </div>
            </div>

            <div class="row justify-content-md-center pb-5 mobile-padding-2">
                <?php
                if (have_rows('connections_subtitle_fifth')) :
                    while (have_rows('connections_subtitle_fifth')) : the_row();
                        $picture_first = get_sub_field('picture_first');
                        ?>
                        <div class="col-6 col-sm-3 col-md-2 pt-4">
                            <div class="connections_logo_first pt-4 pb-2">
                                <img src="<?php echo $picture_first; ?>"/>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </section>

<!-- Our packages -->
<!-- -------------- -->
    <section>
        <section id="over_sixth_part_one">
            <div class="container">
                <div class="row">
                    <?php $our_packages_title = get_field("our_packages_title"); ?>
                    <div class="col">
                        <div class="our_packages_title text-center">
                            <?php echo $our_packages_title; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="over_sixth_part_two"
                 style="background:url(<?php echo get_template_directory_uri() . "/img/fifthsection.png" ?>) no-repeat; background-size: contain;">
            <div class="container" id="bubbleswrapper">
                <div class="row pb-5" id="fifth_row">
                    <div class="col-xs-6 col-sm-4 d-flex">

                        <?php
                        if (have_rows('our_package_first')) :
                            while (have_rows('our_package_first')) : the_row();
                                $title = get_sub_field('title');
                                $footer = get_sub_field('footer');
                                $header = get_sub_field('header');
                                ?>
                                <div class="our_package_first our_package">
                                    <div class="our_package_title text-center title-green">
                                        <?php echo $title; ?>
                                    </div>
                                    <div class="our_package_header text-center">
                                        <?php echo $header; ?>
                                    </div>
                                    <ul class="our_package_list"
                                        style="list-style-image: url(<?php echo get_template_directory_uri() . "/img/tick2.png" ?>);">
                                        <?php
                                        if (have_rows('checkboxes')) :
                                            while (have_rows('checkboxes')) : the_row();
                                                $checkbox_list = get_sub_field('checkbox_list');
                                                ?>
                                                <li class="pb-4">
                                                    <span><?php echo $checkbox_list ?></span>
                                                </li>
                                            <?php
                                            endwhile;
                                        endif;
                                        ?>
                                    </ul>
                                    <hr>
                                    <div class="our_package_footer text-center">
                                        <?php echo $footer; ?>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        endif;
                        ?>

                    </div>
                    <div class="col-xs-6 col-sm-4 d-flex">
                        <?php
                        if (have_rows('our_package_second')) :
                            while (have_rows('our_package_second')) : the_row();
                                $title = get_sub_field('title');
                                $footer = get_sub_field('footer');
                                $header = get_sub_field('header');
                                ?>

                                <div class="our_package_second our_package">
                                    <div class="our_package_title text-center">
                                        <?php echo $title; ?>
                                    </div>
                                    <div class="our_package_header text-center">
                                        <?php echo $header; ?>
                                    </div>
                                    <ul class="our_package_list"
                                        style="list-style-image: url(<?php echo get_template_directory_uri() . "/img/tick.png" ?>);">
                                        <?php
                                        if (have_rows('checkboxes')) :
                                            while (have_rows('checkboxes')) : the_row();
                                                $checkbox_list = get_sub_field('checkbox_list');
                                                ?>
                                                <li class="pb-4">
                                                    <span><?php echo $checkbox_list ?></span>
                                                </li>
                                            <?php
                                            endwhile;
                                        endif;
                                        ?>
                                    </ul>
                                    <hr class="white-hr">
                                    <div class="our_package_footer text-center mb-5">
                                        <?php echo $footer; ?>
                                    </div>
                                </div>

                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                    <div class="col-xs-6 col-sm-4 d-flex">
                        <?php
                        if (have_rows('our_package_third')) :
                        while (have_rows('our_package_third')) :
                        the_row();
                        $title = get_sub_field('title');
                        $footer = get_sub_field('footer');
                        $header = get_sub_field('header');
                        ?>
                        <div class="our_package_third our_package">
                            <div class="our_package_title text-center  title-green">
                                <?php echo $title; ?>
                            </div>
                            <div class="our_package_header text-center">
                                <?php echo $header; ?>
                            </div>
                            <ul class="our_package_list"
                                style="list-style-image: url(<?php echo get_template_directory_uri() . "/img/plus.png" ?>);">
                                <?php
                                if (have_rows('checkboxes')) :
                                    while (have_rows('checkboxes')) : the_row();
                                        $checkbox_list = get_sub_field('checkbox_list');
                                        ?>
                                        <li class="pb-4">
                                            <span><?php echo $checkbox_list ?></span>
                                        </li>
                                    <?php
                                    endwhile;
                                endif;
                                ?>
                            </ul>
                            <hr>
                            <div class="our_package_footer text-center">
                                <?php echo $footer; ?>
                            </div>
                            <div class="our_package_link text-center">
                                Máš zájem? <a href="<?php echo get_page_link(16) ?>">Napiš nám.<a>
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </section>

        <section id="over_sixth_part_three">
            <div class="container">
                <div class="row">
                    <div class="col text-center pb-5">
                        <a href="<?php echo get_page_link(233); ?>" class="our_packages_button">
                            <p class="d-inline-block order-button-child">ceník a specifikace</p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section id="banner_cesta"
             style="background: url(<?php echo get_template_directory_uri() . "/images/cesta_k_zisku.jpg" ?>)">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <p>E-shop je důležitý první krok, ale cesta ke slušnému zisku je dlouhá.</p>
                        <p>Dostaňte své zboží mezi lidi a zvyšujte tržby pomocí online reklamních kanálů.</p>
                    </div>
                    <div class="col-md-3">
                        <a class="button_cesta" href="https://www.marketsoul.cz/project/sluzby-pro-e-shopy/"
                           target="_blank">Zjistit více</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

<!-- Posts Grid -->

    <section id="over_seventh_part">
        <div class="container" id="seventh_container">
            <?php $novinky_title = get_field("novinky_title"); ?>
            <div class="row">
                <div class="col">
                    <div class="connections_title text-center">
                        <?php echo $novinky_title; ?>
                    </div>
                </div>
            </div>
            <div class="card-columns">
                <?php
                if (have_rows('grid_cards')) :
                    while (have_rows('grid_cards')) :
                        the_row();
                        $title = get_sub_field('title');
                        $text = get_sub_field('text');
                        $picture = get_sub_field('picture');
                        ?>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $title; ?></h5>
                                <p class="card-text"><?php echo $text ?></p>
                                <?php if ($picture) { ?>
                                    <img class="card-picture" src="<?php echo $picture; ?>"/>
                                    <?php
                                } ?>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </section>

<?php
}
//wp_reset_query();
?>


<?php get_footer(); ?>
