<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
            echo ' –';
        } ?><?php bloginfo('name'); ?></title>

    <!--<link href="//www.google-analytics.com" rel="dns-prefetch">-->
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KBRLZTH');</script>
    <!-- End Google Tag Manager -->

    <meta name="google-site-verification" content="M9fTEkbDAbfVvHFxdoAK_6WpZvAZvz43MJayaOZhyZw"/>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <meta name="twitter:image" content="<?php echo get_site_url(); ?>/wp-content/uploads/2019/08/05_profil_zakaznika.jpg">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . "/img/favicon.ico" ?>"
          type="image/x-icon">
    <link rel="icon" href="<?php echo get_template_directory_uri() . "/img/favicon.ico" ?>" type="image/x-icon">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFNv098hYoT7cWaZAevKcXFbTKxU6982Q"></script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="wrapper">

    <header id="nav_header">
        <nav class="navbar navbar-light navbar-expand-lg">
            <div id="nav_container" class="container nowrap pt-4 pb-4">

                <a href="<?php echo get_home_url(); ?>" class="navbar-brand">
                <span class="logo d-block">
                    <img src="<?php echo get_template_directory_uri() . "/img/Logo_esoul_minified.svg" ?>" class="exclude_lazyload">
                </span>
                </a>

                <button class="navbar-toggler custom-toggler" data-toggle="collapse" data-target="#navbarMenu"
                        aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarMenu">
                    <?php echo wp_nav_menu(array('menu' => 2, 'menu_class' => 'primary_nav')); ?>
                    <a href="https://msesoul.marketsoul.cz/" target="_blank"
                       class="nav-link blue_button nav-item-last">VYZKOUŠEJ DEMO</a>
                </div>
            </div>

        </nav>
    </header>










