(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        // Scale function (still to be added)

        //Slider_second
        $('#aa_over_firstpart .row .col #slider2 ').slick({
            //infinite: true,
            dots: true,
            arrows: true,
            slidesToShow: 1,
            //slidesToScroll: 1,
            prevArrow: false,
            nextArrow: $("#slickRight_main_home2 button"),
            fade: true,
            cssEase: 'linear',
            speed: 500,
            //autoplay: true,
            //autoplaySpeed: 2000,
        });

        function slide_on_scrool2(){
            const slider = $("#aa_over_firstpart .row .col #slider2");

            slider.on('wheel', (function(e) {

                var slideCount = $(this)[0].slick["slideCount"];
                var currentIndex = $(this).slick("slickCurrentSlide");
                var totalSildeToShow =  $(this)[0].slick.options["slidesToShow"];

                if (e.originalEvent.deltaY < 0) {
                    //e.preventDefault();   -- prevents body from scrolling
                    //$(this).slick('slickPrev');
                } else {
                    if (slideCount - totalSildeToShow == currentIndex)
                        return;
                    e.preventDefault();
                    $(this).slick('slickNext')
                }
            }));
        }
        slide_on_scrool2();

    });

})(jQuery, this);