(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';

        $(document).ready(function() {

        var hash = location.hash.replace('#','');
        var elem = window.location.hash;

        if(hash != ''){
            $('html,body').animate({
              scrollTop: $(""+elem+"").offset().top - 200
            }, 1000);
            
            $(elem).next(".colapse-adjustment").addClass("show");
           }

        });
		
		// DOM ready, take it away

        // Scale function (still to be added)


        // Slider_first
        $('#over_fourth_part .row .col #slider ').slick({
            //infinite: true,
            dots: true,
            arrows: true,
            slidesToShow: 1,
            //slidesToScroll: 1,
            prevArrow: false,
            nextArrow: $("#slickRight_main_home button"),
            fade: true,
            cssEase: 'linear',
            speed: 500,
            //customPaging : function(slider, i) {
            //var thumb = $(slider.$slides[i]).data();
            //return '<a>0'+i+'</a>';
            //}
            //autoplay: true,
            //autoplaySpeed: 2000,
        });

        //Slider on scroll functionality  == works when mouse is over the slider
        function slide_on_scrool(){
            const slider = $("#over_fourth_part .row .col #slider");

            slider.on('wheel', (function(e) {

                var slideCount = $(this)[0].slick["slideCount"];
                var currentIndex = $(this).slick("slickCurrentSlide");
                var totalSildeToShow =  $(this)[0].slick.options["slidesToShow"];

                if (e.originalEvent.deltaY < 0) {
                    //e.preventDefault();   -- prevents body from scrolling
                    //$(this).slick('slickPrev');
                } else {
                    if (slideCount - totalSildeToShow == currentIndex)
                        return;
                    e.preventDefault();
                    $(this).slick('slickNext')
                }
            }));
        }
        slide_on_scrool();

// Function to make objects bigger, will work only for a bigger screens

//Header fades while on scroll---------------------
        //$(document).on("scroll", function(){
           // var startScroll = $(document).scrollTop();
            //if(!$("#nav_header").hasClass("fadeHeader")){
             //   $("#nav_header").addClass("fadeHeader");
               // return;
            //}
            //setTimeout(function(){
              //  if($(document).scrollTop() == startScroll){
              //      $("#nav_header").removeClass("fadeHeader");
              //  }
            //}, 200);
        //});

//moving city background footer-------------------
        $(function(){
            var x = 0;
            setInterval(function(){
                x+=0.5;
                $('.footer_city').css('background-position', x + 'px 0');
            }, 10);
        });

        //moving city background header
        if($(window).width() >= 1024) {
            $(function () {
                var x = 0;
                setInterval(function () {
                    x += 1;
                    $('.header_city').css('background-position', x + 'px 0');
                }, 10);
            });
        }

        //disincluding child elements from animate css function
        $().ready(function(){
            $('#order_button:hover').find(":not('p')").animate({
                scale : [1,1.3]
            },1000); });

        // Sticky header on scroll functionality to add
        // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myFunction()};

        // Get the header
        var header = document.getElementById("nav_header");

        // Get the offset position of the navbar
        var sticky = header.offsetTop;

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        }

        /* ju,ping dot in nav menu */
        $(".co_je_esoul").click(function(){
            $(".nav-item").each(function(){
              $(this).removeClass("nav-item--active");
              $(".co_je_esoul").addClass("nav-item--active");
            });
          });

        if (window.location.href.indexOf("#co_je_esoul") != -1){
            
            $(".nav-item").each(function(){
              $(this).removeClass("nav-item--active");
            });
            $(".co_je_esoul").addClass("nav-item--active");
        }
	});

    var $animation_elements = $('.image_fade_left');
    var $window = $(window);

    function check_if_in_view() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);

        $.each($animation_elements, function() {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);

            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
                $element.removeClass('animated');
            }
            // If you want to repeat animation each time on scroll
            // else {
            //     $element.removeClass('in-view');
            // }
        });
    }

    /** Anchor smooth scroll */
    $(document).ready(function () {
        $("a").on('click', function() {
            var hash = $(this).attr('href');
            if(/^#/.test(hash)) {
                if (hash !== '#') {
                    $('html, body').stop().animate({
                        scrollTop: $('#'+hash.split('#')[1]).offset().top
                    }, 1000, function () {});
                    return false; // Disable adding hash to URL
                }
            }
        });
    });
    
    /** Tabs */
    $(document).ready(function () {
        $('.tabs > ul > li').click(function () {
            var identifier = $(this).data('toggle');
            var tabs = $(this).parent().parent();
            // Find tab and open
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings().removeClass('active');
                tabs.find(identifier).fadeToggle().siblings().hide();
            }
        });
    });

    $(document).ready(function() {
        $('#nav_header').on('click', function() {
            $(this).toggleClass('overlay-is-navbar-collapse');
        });
    });

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');

})(jQuery, this);

// appeared logos
    AOS.init();




