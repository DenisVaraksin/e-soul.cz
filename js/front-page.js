(function ($, root, undefined) {

    $(function () {

        'use strict';

//Sightly move people with mousemove
        var myContainer = $('#moving_people');
        var sensitivityMultiplier = 0.02;
        var wrapperOffset = myContainer.offset();
        var CenterX = wrapperOffset.left + (myContainer.width() / 2);
        var CenterY = wrapperOffset.top + (myContainer.height() / 2);

        $(window).mousemove(function (e) {
            var mouseX = e.pageX;
            var mouseY = e.pageY;
            doAwesomeness(mouseX, mouseY);
        });

        function doAwesomeness(mouseX, mouseY) {
            var RelX = (mouseX - CenterX) * sensitivityMultiplier;
            var RelY = ((mouseY - CenterY) * -1) * sensitivityMultiplier;
            myContainer.css('-webkit-transform', 'translateY(' + RelX + 'px) translateX(' + RelY + 'px)');
            myContainer.css('transform', 'translateY(' + RelX + 'px) translateX(' + RelY + 'px)');
        }

        //make bubbles elements SCALE

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        // Sticky header on scroll functionality to add
        // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myFunction()};

        // Get the header
        var header = document.getElementById("nav_header");

        // Get the offset position of the navbar
        var sticky = header.offsetTop;

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }

            if($(window).width() >= 1024){
                const bubbles = document.getElementById("bubbleswrapper");

                if(bubbles.getBoundingClientRect().top <= 500 && !bubbles.classList.contains(("active"))){
                    bubbles.className += " active";
                    console.log("reached");
                }
            }
        }
        //-------------- changing header style when nav menu collapsed ---------------   //
         $('.navbar').on('show.bs.collapse', function () {
                var header2 = document.getElementById("nav_header");
                $(header2).addClass('overlay-is-navbar-collapse');
            });
         $('.navbar').on('hide.bs.collapse', function () {
                var header2 = document.getElementById("nav_header");
                $(header2).removeClass('overlay-is-navbar-collapse');
            });




        //-------------- smooth scrolling ---------------   //
        (function(document, history, location) {
            var HISTORY_SUPPORT = !!(history && history.pushState);

            var anchorScrolls = {
                ANCHOR_REGEX: /^#[^ ]+$/,
                OFFSET_HEIGHT_PX: 50,

                /**
                 * Establish events, and fix initial scroll position if a hash is provided.
                 */
                init: function() {
                    this.scrollToCurrent();
                    $(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
                    $('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
                },

                /**
                 * Return the offset amount to deduct from the normal scroll position.
                 * Modify as appropriate to allow for dynamic calculations
                 */
                getFixedOffset: function() {
                    return this.OFFSET_HEIGHT_PX;
                },

                /**
                 * If the provided href is an anchor which resolves to an element on the
                 * page, scroll to it.
                 * @param  {String} href
                 * @return {Boolean} - Was the href an anchor.
                 */
                scrollIfAnchor: function(href, pushToHistory) {
                    var match, anchorOffset;

                    if(!this.ANCHOR_REGEX.test(href)) {
                        return false;
                    }

                    match = document.getElementById(href.slice(1));

                    if(match) {
                        anchorOffset = $(match).offset().top - this.getFixedOffset();
                        $('html, body').animate({ scrollTop: anchorOffset});

                        // Add the state to history as-per normal anchor links
                        if(HISTORY_SUPPORT && pushToHistory) {
                            history.pushState({}, document.title, location.pathname + href);
                        }
                    }

                    return !!match;
                },

                /**
                 * Attempt to scroll to the current location's hash.
                 */
                scrollToCurrent: function(e) {
                    if(this.scrollIfAnchor(window.location.hash) && e) {
                        e.preventDefault();
                    }
                },

                /**
                 * If the click event's target was an anchor, fix the scroll position.
                 */
                delegateAnchors: function(e) {
                    var elem = e.target;

                    if(this.scrollIfAnchor(elem.getAttribute('href'), true)) {
                        e.preventDefault();
                    }
                }
            };

            $(document).ready($.proxy(anchorScrolls, 'init'));
        })(window.document, window.history, window.location);

        // --------------------------------------------------------------------//


        //not only goes to anchor but also opens a video
        $("#esoul_nav_video_link").click(function(e) {
            setTimeout(function(){
                $("#link_size").trigger("click");
            }, 400)
        });
        // for small screens directs to link with the video
        $("#link_size").click(function(e) {
            if ($(window).width() > 960) {
                $("#myModal").modal();
            }
            else {
                e.preventDefault();
                window.open('https://www.marketsoul.cz/','_blank');  // replace the link with url needed
                //window.location.replace("http://www.w3schools.com");
            }
        });

        $(".close").click(function() {
            $("#myModal").modal('hide');
        })

        // -------------- Stops video when modal is closed ---------------------------//
        $("#myModal").on('hidden.bs.modal', function (e) {
            $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
        });

    });

})(jQuery);
