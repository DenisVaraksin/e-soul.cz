<?php
/*------------------------------------*\
	Theme Support
\*------------------------------------*/

define("THEME_VERSION", "65"); //use to refresh CSS and JS cache if needed

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('photo', 1440, '', true); // Container width Thumbnail
    add_image_size('large', 750, '', true); // Large Thumbnail
    add_image_size('medium', 500, '', true); // Medium Thumbnail
    add_image_size('small', 250, '', true); // Small Thumbnail

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('theme', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
// navigation
function theme_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function theme_styles()
{
	wp_enqueue_style('bs', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(), THEME_VERSION, 'all');

	wp_enqueue_style('bs', '//magnific-popup/magnific-popup.css', array(), THEME_VERSION, 'all');
	wp_enqueue_style('aos', '//unpkg.com/aos@2.3.1/dist/aos.css', array(), THEME_VERSION, 'all');
	wp_enqueue_style('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), THEME_VERSION, 'all');
	wp_enqueue_style('animatecss', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css', array(), THEME_VERSION, 'all');

    wp_register_style('theme', get_template_directory_uri() . '/style.css', array(), THEME_VERSION, 'all');
    wp_enqueue_style('theme');

    wp_register_style('main', get_template_directory_uri() . '/css/main.css', array(), THEME_VERSION, 'all');
    wp_enqueue_style('main');

    //additional css
	wp_register_style('footer', get_template_directory_uri() . '/css/footer.css', array(), THEME_VERSION, 'all');
	wp_enqueue_style('footer');

    wp_register_style('co-je-esoul', get_template_directory_uri() . '/css/co-je-esoul.css', array(), THEME_VERSION, 'all');
    if( is_page( 357 ) ) : wp_enqueue_style( 'co-je-esoul' ); endif;

	wp_register_style('pro-agentury', get_template_directory_uri() . '/css/pro-agentury.css', array(), THEME_VERSION, 'all');
	if( is_page( 14 ) ) : wp_enqueue_style( 'pro-agentury' ); endif;

	wp_register_style('data-protection', get_template_directory_uri() . '/css/data-protection.css', array(), THEME_VERSION, 'all');
	if( is_page( 194 ) ) : wp_enqueue_style( 'data-protection' ); endif;  //page ID 195 - on localhost and 194 on live site

	wp_register_style('order-demo', get_template_directory_uri() . '/css/order-demo.css', array(), THEME_VERSION, 'all');
	if( is_page( 16 ) ) : wp_enqueue_style( 'order-demo' ); endif;

    wp_register_style('thank-you', get_template_directory_uri() . '/css/thank-you.css', array(), THEME_VERSION, 'all');
    if( is_page( 349 ) ) : wp_enqueue_style( 'thank-you' ); endif;

	wp_register_style('cenik', get_template_directory_uri() . '/css/cenik.css', array(), THEME_VERSION, 'all');
	if( is_page( 233 ) ) : wp_enqueue_style( 'cenik' ); endif;              //page ID 24 - on localhost and 233 on live site


    wp_register_style('responsive', get_template_directory_uri() . '/css/responsive.css', array(), THEME_VERSION, 'all');
    wp_enqueue_style('responsive');

    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
	    wp_enqueue_script('bsjs', "//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js", array('jquery'), THEME_VERSION, true);

	    wp_enqueue_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), THEME_VERSION, true);

	    wp_enqueue_script('aosjs', "//unpkg.com/aos@2.3.1/dist/aos.js", array('jquery'), THEME_VERSION, true);


	    wp_register_script('themescripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), THEME_VERSION, true);
        wp_enqueue_script('themescripts');

    }

    wp_register_script('mapa_js', get_template_directory_uri() . '/js/mapa.js', array('jquery'), THEME_VERSION, true);
    wp_enqueue_script('mapa_js');

    if(is_front_page()) {
	    wp_register_script('hpjs', get_template_directory_uri() . '/js/front-page.js', array('jquery'), THEME_VERSION, true);
	    wp_enqueue_script('hpjs');
    }

    if(get_the_ID() == 14) {
	    wp_register_script('additonalscript', get_template_directory_uri() . '/js/about-agency-script.js', array('jquery'), THEME_VERSION, true);
	    wp_enqueue_script('additonalscript');
    }


	if(get_the_ID() == 16) {
		wp_register_script('additonalscript2', get_template_directory_uri() . '/js/order-demo.js', array('jquery'), THEME_VERSION, true);
		wp_enqueue_script('additonalscript2');
	}

    wp_enqueue_style( 'lightbox', get_template_directory_uri() . '/css/lightbox.css', array(), THEME_VERSION , 'all');
    wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/js/lightbox.js', array ( 'jquery' ), THEME_VERSION , false); 

    // Example of conditional script
    // if (is_page('pagenamehere')) {
    //     wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), THEME_VERSION, true); 
    //     wp_enqueue_script('scriptname');
    // }
}



// Register navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'theme'), // Main Navigation
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    register_sidebar(array(
        'name' => __('Widget Area 1', 'theme'),
        'description' => __('Description for this widget-area...', 'theme'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Widget Area 2', 'theme'),
        'description' => __('Description for this widget-area...', 'theme'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'theme') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function themegravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function themecomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/
// Add Actions
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'theme_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'themegravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

function create_post_type_html5()
{
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('CustomPosts', 'theme'),
            'singular_name' => __('CustomPost', 'theme'),
            'add_new' => __('Add New', 'theme'),
            'add_new_item' => __('Add New Post', 'theme'),
            'edit' => __('Edit', 'theme'),
            'edit_item' => __('Edit Post', 'theme'),
            'new_item' => __('New Post', 'theme'),
            'view' => __('View', 'theme'),
            'view_item' => __('View Post', 'theme'),
            'view_items' => __('View Posts', 'theme'),
            'search_items' => __('Search Posts', 'theme'),
            'not_found' => __('No posts found', 'theme'),
            'not_found_in_trash' => __('No posts found in Trash', 'theme')
        ),
        'public' => true,
        'can_export' => true, // Allows export in Tools > Export
        'show_in_rest' => false, // Set this to true if you want to use Gutenberg with this CPT
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'menu_icon' => 'dashicons-welcome-write-blog',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'revisions',
            // 'page-attributes', // Allows menu_order and other page-like attributes
        ), 
    ));
}

function create_posttype() {
	register_post_type( 'box_pluses',  //pluses custom post type
		array(
			'labels' => array(
				'name' => __( 'Esoul Pluses' ),
				'singular_name' => __( 'Esoul Pluses' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'title', 'thumbnail' ),
			'show_in_rest' => false,
		)
	);

	register_post_type( 'what_esoul',   //homepage
		array(
			'labels' => array(
				'name' => __( 'Homepage' ),
				'singular_name' => __( 'Homepage' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'title', 'thumbnail' ),
			'show_in_rest' => false,
		)
	);

	register_post_type( 'pro_agentury',   //pro agentury
		array(
			'labels' => array(
				'name' => __( 'Pro agentury' ),
				'singular_name' => __( 'Pro agentury' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'title', 'thumbnail' ),
			'show_in_rest' => false,
		)
	);

	register_post_type( 'order_demo',   //order demo
		array(
			'labels' => array(
				'name' => __( 'Order demo' ),
				'singular_name' => __( 'Order Demo' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'title', 'thumbnail' ),
			'show_in_rest' => false,
		)
	);

	register_post_type( 'cenik',   //cenik page
		array(
			'labels' => array(
				'name' => __( 'Cenik' ),
				'singular_name' => __( 'Cenik' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'title', 'thumbnail' ),
			'show_in_rest' => false,
		)
	);
}
add_action( 'init', 'create_posttype' );



//Edit post link to open in new tab
add_filter( 'edit_post_link', function( $link, $post_id, $text )
{
    // Add the target attribute 
    if( false === strpos( $link, 'target=' ) )
        $link = str_replace( '<a ', '<a target="_blank" ', $link );

    return $link;
}, 10, 3 );

add_action('admin_head', 'wbt_admin_css');

function wbt_admin_css() {
  echo '<style>
  .wp-block {
        max-width: 1300px;
    }
  </style>';
}

/**
* Create Logo Setting and Upload Control
*/
function brand_logo_in_customizer($wp_customize) {
    $wp_customize->add_setting('header_logo');
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo',
    array(
        'label' => 'Nahrát logo',
        'section' => 'title_tagline',
        'settings' => 'header_logo',
    ) ) );

}
add_action('customize_register', 'brand_logo_in_customizer');

function my_acf_init() {
  
  acf_update_setting('google_api_key', 'AIzaSyDHAcIB656pDwauH0f0JkXuQg8ILM0fVfw');
}

add_action('acf/init', 'my_acf_init');

function get_tabs($titles, $contents, $identifier = null)
{

    $i = 0; // Iteration for titles
    $tabs = '<div class="tabs"' . (isset($identifier) ? 'id="' . $identifier . '"' : null) . '>';
    $tabs .= '<ul>';
    foreach ($titles as $title) {
        if (!empty($contents[$i])) {
            $tabs .= '<li class="' . ($i == 0 ? 'active' : null) . '" data-toggle="#tab_' . $i . '">' . $title . '</li>';
        }
        $i++;
    }
    $tabs .= '</ul>';
    $tabs .= '<div class="tabs_content">';
    $i = 0; // Iteration for content
    foreach ($contents as $content) {
        if (!empty($contents[$i])) {
            $tabs .= '<div class="tab ' . ($i == 0 ? 'open' : null) . '" id="tab_' . $i . '">' . $content . '</div>';
        }
        $i++;
    }
    $tabs .= '</div>';
    $tabs .= '</div>';
    return $tabs;

}
add_action('get_tabs', 'get_tabs');
